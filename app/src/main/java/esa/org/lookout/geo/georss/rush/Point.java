package esa.org.lookout.geo.georss.rush;

import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;


public class Point {

    private String prefix;
    private String text;
    private LatLng latLng;
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getPrefix() {
        return prefix;
    }


    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {

        this.text = text;
        latLngFromText();
    }


    public LatLng getLatLng() {
        return latLng;
    }


    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }


    private void latLngFromText() {
        String[] latLngStr = text.split(" ");
        latLng = new LatLng(Double.valueOf(latLngStr[0]), Double.valueOf(latLngStr[1]));
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
