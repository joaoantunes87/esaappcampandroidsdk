package esa.org.lookout.geo.detector;

import java.util.Calendar;

/**
 * Simple class to represent a value for a specific latitude and longitude.
 * <p/>
 * Created by robknapen on 11/09/14.
 */
public class GeoValue {
    private double latitude;
    private double longitude;
    private float value;
    private Calendar date;


    private boolean coordEqual(double val1, double val2) {
        return (Math.abs(val1 - val2) < 0.00001);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoValue)) return false;

        GeoValue geoValue = (GeoValue) o;

        if (!coordEqual(geoValue.latitude, latitude)) return false;
        if (!coordEqual(geoValue.longitude, longitude)) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


    public double getLatitude() {
        return latitude;
    }


    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public double getLongitude() {
        return longitude;
    }


    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public float getValue() {
        return value;
    }


    public void setValue(float value) {
        this.value = value;
    }


    public Calendar getDate() {
        return date;
    }


    public void setDate(Calendar date) {
        this.date = date;
    }
}
