package esa.org.lookout.geo.detector;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * An event detected by the HazardDetector based on hotspots and tracking
 * of them. The event might by a real hazard, or a false positive. The
 * detector model tries to decide between that based on the spreading of
 * the hotspots.
 * <p/>
 * Created by robknapen on 10/09/14.
 */
public class HazardEvent {

    // threshold distance use to consider hotspots near to other hotspots in the event
    private static final float NEAR_DISTANCE_METERS = 500;

    // parameters to detect critical event status
    private static final int HOTSPOTS_COUNT_THRESHOLD = 5;

    private String id;
    private String label;
    private Date created;
    private Date lastModified;
    private boolean isReported;

    private List<GeoValue> hotspots = new ArrayList<GeoValue>();


    public static HazardEvent create(String label) {
        HazardEvent event = new HazardEvent();
        event.setLabel(label);
        return event;
    }


    private HazardEvent() {
        id = UUID.randomUUID().toString();
        label = "New Event";
        created = new Date();
        hotspots.clear();
        isReported = false;
        updateLastModified();
    }


    private void updateLastModified() {
        lastModified = Calendar.getInstance().getTime();
    }


    public void clearHotspots() {
        hotspots.clear();
        updateLastModified();
    }


    public boolean hasHotspots() {
        return hotspots.size() > 0;
    }


    public int hotspotsCount() {
        return hotspots.size();
    }


    public void addHotspot(GeoValue hotspot) {
        if (!containesHotspot(hotspot)) {
            if (hotspots.add(hotspot)) {
                updateLastModified();
            }
        }
    }


    public void removeHotspot(GeoValue hotspot) {
        if (hotspots.remove(hotspot)) {
            updateLastModified();
        }
    }


    public boolean isCritical() {
        return hotspotsCount() >= HOTSPOTS_COUNT_THRESHOLD;
    }


    public boolean containesHotspot(GeoValue hotspot) {
        return hotspots.contains(hotspot);
    }


    public boolean isNearHotspot(GeoValue hotspot) {
        double lat = hotspot.getLatitude();
        double lon = hotspot.getLongitude();

        for (GeoValue gv : hotspots) {
            if ((lat > (gv.getLatitude() - NEAR_DISTANCE_METERS)) &&
                    (lat < (gv.getLatitude() + NEAR_DISTANCE_METERS)) &&
                    (lon > (gv.getLongitude() - NEAR_DISTANCE_METERS)) &&
                    (lon < (gv.getLongitude() + NEAR_DISTANCE_METERS))
                    ) {
                return true;
            }
        }
        return false;
    }


    public void updateHotspotValue(GeoValue hotspot) {
        if (containesHotspot(hotspot)) {
            int index = hotspots.indexOf(hotspot);
            hotspots.get(index).setValue(hotspot.getValue());
        }
    }


    public void decayHotspots(List<GeoValue> newHotspots) {
        // if event has hotspots that are no longer part of the (new) hotspots, remove them
        // TODO: this can be made more intelligent
        Iterator<GeoValue> iter = hotspots.iterator();
        while (iter.hasNext()) {
            GeoValue gv = iter.next();
            if (!newHotspots.contains(gv)) {
                iter.remove();
            }
        }
    }


    @Override
    public String toString() {
        return "HazardEvent{" +
                "id='" + id + '\'' +
                ", label='" + label + '\'' +
                ", created=" + created +
                ", lastModified=" + lastModified +
                ", hotspots=" + hotspots +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HazardEvent)) return false;

        HazardEvent that = (HazardEvent) o;

        if (!id.equals(that.id)) return false;

        return true;
    }


    @Override
    public int hashCode() {
        return id.hashCode();
    }


    public String getId() {
        return id;
    }


    public String getLabel() {
        return label;
    }


    public void setLabel(String label) {
        this.label = label;
        updateLastModified();
    }


    public Date getCreated() {
        return created;
    }


    public Date getLastModified() {
        return lastModified;
    }


    public boolean isReported() {
        return isReported;
    }


    public void setReported(boolean isReported) {
        this.isReported = isReported;
    }
}
