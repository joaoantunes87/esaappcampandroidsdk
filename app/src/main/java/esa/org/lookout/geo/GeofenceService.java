package esa.org.lookout.geo;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationStatusCodes;

public class GeofenceService implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationClient.OnAddGeofencesResultListener {

    // for debugging
    private static final String TAG = GeofenceService.class.getSimpleName();
    private static final boolean D = true;

    private Activity activity;
    private LocationClient locationClient;
    private PendingIntent geofenceRequestIntent;
    private GeofenceUtils.REQUEST_TYPE requestType;
    private boolean inProgress;


    private static GeofenceService instance;


    private GeofenceService() {
        inProgress = false;
    }


    public static void start(Activity activity) {
        if (instance == null) {
            if (D) {
                Log.d(TAG, "Starting Geofence Service");
            }
            instance = new GeofenceService();
            instance.setActivity(activity);
            instance.connect();
        }
    }


    public static GeofenceService getInstance() {
        if (instance == null) {
            instance = new GeofenceService();
        }
        return instance;
    }


    public void stop() {
        if (D) {
            Log.d(TAG, "Stopping Geofence Service");
        }
        // TODO: clean-up, if needed
    }


    public void updateGeofences() {
        instance.connect();
    }


    private void setActivity(Activity activity) {
        this.activity = activity;
    }


    private void connect() {
        requestType = GeofenceUtils.REQUEST_TYPE.ADD;
        if (!GeofenceUtils.servicesConnected(activity)) {
            return;
        }

        locationClient = new LocationClient(activity, this, this);
        if (!inProgress) {
            if (D) {
                Log.d(TAG, "Requesting client connection");
            }
            inProgress = true;
            locationClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        // TODO: add geofences from local storage

/*
        Log.d(TAG, "==== Adding these Geofences ====");
        for(SimpleGeofence g : GeofenceUtils.mGeofenceList) {
            Log.d(TAG, "ID: " + g.getId());
            Log.d(TAG, "Lat: " + g.getLat());
            Log.d(TAG, "Lon: " + g.getLon());
            Log.d(TAG, "Radius: " + g.getRadius());
            Log.d(TAG, "Transition: " + g.getTransitionType());
            Log.d(TAG, "===============================");
        }

        switch (mRequestType) {
            case ADD:
                if(GeofenceUtils.mGeofenceList.size() > 0) {
                    mLocationClient.addGeofences(GeofenceUtils.toGeofenceList(), getTransitionPendingIntent(), this);
                    Log.d(TAG, "Geofencing service addition requested");
                } else {
                    mInProgress = false;
                    mLocationClient.disconnect();
                    Log.d(TAG, "No Geofences found");
                }
                break;
        }

*/
    }


    private PendingIntent getTransitionPendingIntent() {
        if (geofenceRequestIntent == null) {
            Intent intent = new Intent(activity, GeofenceIntentService.class);
            geofenceRequestIntent = PendingIntent.getService(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return geofenceRequestIntent;
    }


    @Override
    public void onDisconnected() {
        // TODO: remove geofences?
        if (D) {
            Log.d(TAG, "Services disconnected");
        }
        inProgress = false;
        locationClient = null;
    }


    @Override
    public void onAddGeofencesResult(int i, String[] strings) {
        if (i == LocationStatusCodes.SUCCESS) {
            if (D) {
                Log.d(TAG, "Successfully added Geofences");
            }
        } else {
            Log.e(TAG, "Problem adding Geofences");
        }
        inProgress = false;
        locationClient.disconnect();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        inProgress = false;
        Log.e(TAG, "Connection to services failed");
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(
                        activity,
                        GeofenceUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException ex) {
                Log.e(TAG, ex.getLocalizedMessage(), ex);
            }
        }
    }
}
