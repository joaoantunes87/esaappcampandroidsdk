package esa.org.lookout.geo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A region of interest for the app. This can be the direct location of the user
 * or an additional region the user is interested to know the risk status for.
 * <p/>
 * Regions of interest currently are circular, since this is the only type Google
 * GeoFence functionality supports at the moment.
 * <p/>
 * Created by Rob Knapen on 09/09/14.
 */
public class InterestRegion implements Parcelable {

    public static final float DEFAULT_RADIUS = 50;

    // interest zone information
    private String label = "New Region of Interest";
    private String description = "";

    // circular region location
    private double latitude;
    private double longitude;
    private float radius;


    public static InterestRegion createRegion(String label, String description, double latitude, double longitude, float radius) {
        InterestRegion region = new InterestRegion();
        region.setLabel(label);
        region.setDescription(description);
        region.setLatitude(latitude);
        region.setLongitude(longitude);
        region.setRadius(radius);
        return region;
    }


    public InterestRegion() {
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(description);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeFloat(radius);
    }


    public InterestRegion(Parcel in) {
        label = in.readString();
        description = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        radius = in.readFloat();
    }


    public static final Parcelable.Creator<InterestRegion> CREATOR = new Parcelable.Creator<InterestRegion>() {
        public InterestRegion createFromParcel(Parcel in) {
            return new InterestRegion(in);
        }


        public InterestRegion[] newArray(int size) {
            return new InterestRegion[size];
        }
    };


    @Override
    public String toString() {
        return "InterestRegion{" +
                "label='" + label + '\'' +
                ", description='" + description + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                '}';
    }


    private boolean coordEqual(double val1, double val2) {
        return (Math.abs(val1 - val2) < 0.00001);
    }


    private boolean radiusEqual(float val1, float val2) {
        return (Math.abs(val1 - val2) < 0.1);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InterestRegion)) return false;

        InterestRegion that = (InterestRegion) o;

        if (!coordEqual(that.latitude, latitude)) return false;
        if (!coordEqual(that.longitude, longitude)) return false;
        if (!radiusEqual(that.radius, radius)) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (radius != +0.0f ? Float.floatToIntBits(radius) : 0);
        return result;
    }


    public String getLabel() {
        return label;
    }


    public void setLabel(String label) {
        this.label = label;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public double getLatitude() {
        return latitude;
    }


    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public double getLongitude() {
        return longitude;
    }


    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public float getRadius() {
        return radius;
    }


    public void setRadius(float radius) {
        this.radius = radius;
    }
}
