package esa.org.lookout.geo.georss.maps;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

public class Guid {

    private String text;
    private String isPermaLink;
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


    public String getIsPermaLink() {
        return isPermaLink;
    }


    public void setIsPermaLink(String isPermaLink) {
        this.isPermaLink = isPermaLink;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
