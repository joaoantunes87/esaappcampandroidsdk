package esa.org.lookout.geo;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import esa.org.lookout.backend.InstallationManagerSingleton;
import esa.org.lookout.geo.georss.rush.Feeds;
import esa.org.lookout.geo.georss.rush.Item;

/**
 * Manages the collections of lookout and interest regions, and handles the
 * geofencing operations.
 * <p/>
 * Created by Rob Knapen on 09/09/14.
 */
public class RegionManager {

    public static final int DEFAULT_RADIUS = 40;

    private static final String TAG = RegionManager.class.getSimpleName();
    private static final boolean D = true;


    private List<LookoutRegion> lookoutRegions = new ArrayList<LookoutRegion>();
    private List<InterestRegion> interestRegions = new ArrayList<InterestRegion>();


    public RegionManager() {
    }


    public List<InterestRegion> getInterestRegions() {
        return interestRegions;
    }


    public List<LookoutRegion> getLookoutRegions() {
        return lookoutRegions;
    }


    public void startGeofenceService(Activity activity) {
        if (D) {
            Log.d(TAG, "Starting Geofence Service for RegionManager");
        }
        GeofenceService.start(activity);
    }


    public GeofenceService getGeofenceService() {
        return GeofenceService.getInstance();
    }


    public void stopGeofenceService() {
        if (D) {
            Log.d(TAG, "Stopping Geofence Service for RegionManager");
        }
        getGeofenceService().stop();
    }


    public LookoutRegion addLookoutRegion(String label, double latitude, double longitude, float radius) {
        LookoutRegion region = LookoutRegion.createRegion(label, latitude, longitude, radius);
        if (!lookoutRegions.contains(region)) {
            lookoutRegions.add(region);
        } else {
            int index = lookoutRegions.indexOf(region);
            region = lookoutRegions.get(index);
        }
        return region;
    }


    public void addLookoutRegions(Feeds feeds) {
        List<Item> lookoutGeoRSSHazards = feeds.getRss().getChannel().getItem();
        for (int i = 0; i < lookoutGeoRSSHazards.size(); i++) {
            LookoutRegion lr = addLookoutRegion(lookoutGeoRSSHazards.get(i).getTitle(),
                    lookoutGeoRSSHazards.get(i).getPoint().getLatLng().latitude,
                    lookoutGeoRSSHazards.get(i).getPoint().getLatLng().longitude, 100);

            lr.setDescription(lookoutGeoRSSHazards.get(i).getDescription());
            lr.setAlertStatus(LookoutRegion.ALERT_STATUS_RED);
        }
    }


    public LookoutRegion getLookoutRegionAtIndex(int i) {
        return lookoutRegions.get(i);
    }


    public boolean removeLookoutRegion(LookoutRegion region) {
        return lookoutRegions.remove(region);
    }


    public InterestRegion addInterestRegion(String label, double latitude, double longitude, float radius) {
        InterestRegion region = InterestRegion.createRegion(label, "", latitude, longitude, radius);
        if (!interestRegions.contains(region)) {
            interestRegions.add(region);
            InstallationManagerSingleton.INSTANCE.addInterestRegion(region);
        } else {
            int index = interestRegions.indexOf(region);
            region = interestRegions.get(index);
        }
        return region;
    }


    public InterestRegion addSpecialInterestRegion(String label, double latitude, double longitude, float radius) {
        InterestRegion region = InterestRegion.createRegion(label, "", latitude, longitude, radius);
        if (!interestRegions.contains(region)) {
            interestRegions.add(0, region);
            InstallationManagerSingleton.INSTANCE.addInterestRegion(region);
        } else {
            int index = interestRegions.indexOf(region);
            region = interestRegions.get(index);
        }
        return region;
    }


    public InterestRegion getInterestRegionAtIndex(int i) {
        return interestRegions.get(i);
    }


    public boolean removeInterestRegion(InterestRegion region) {
        return interestRegions.remove(region);
    }

}
