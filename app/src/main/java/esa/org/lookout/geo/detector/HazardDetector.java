package esa.org.lookout.geo.detector;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import esa.org.lookout.backend.LandSurfaceTemperatureManagerSingleton;
import esa.org.lookout.backend.OnSuccessCallback;
import esa.org.lookout.geo.InterestRegion;

/**
 * Simple hazard detector model - To Be Improved!
 * <p/>
 * TODO: improve the working of the model and the distribution of the data
 * <p/>
 * More devices in the same (overlapping) interest region should spread the data
 * so that things are not calculated twice or more and to minimize the amount of
 * data requests.
 * <p/>
 * Created by robknapen on 10/09/14.
 */
public class HazardDetector {

    // simple detector parameters
    private static final float HOTSPOT_TEMP_DIFF_THRESHOLD_DEGREES = 15;

    // for debugging
    private static final String TAG = HazardDetector.class.getSimpleName();
    private static final boolean D = true;
    private static final boolean DEBUG_RUNS = true;
    private static final long DEBUG_TIMESTEP_MSEC = 30 * 60 * 1000; // 30 minutes

    private static final long DATA_RETRIEVAL_TIMESTEP_RANGE_MSEC = 5 * 60 * 1000; // 5 minutes

    private Date timestep;
    private InterestRegion interestRegion;
    private float initialRegionAverageValue = Float.NaN;
    private List<HazardEvent> events = new ArrayList<HazardEvent>();


    public static HazardDetector create(InterestRegion interestRegion) {
        HazardDetector hd = new HazardDetector();
        hd.interestRegion = interestRegion;
        return hd;
    }


    private HazardDetector() {
        if (DEBUG_RUNS) {
            // sample date from Monday Sept 15, 2014
            // starting at 2:00, with 30 min interval
            Calendar cal = Calendar.getInstance();
            cal.set(2014, Calendar.SEPTEMBER, 15, 0, 0);
            timestep = cal.getTime();
        } else {
            timestep = Calendar.getInstance().getTime();
        }
    }


    /**
     * Initialises the detector model.
     *
     * @param interestRegion detector will process
     */
    public void prepare(InterestRegion interestRegion) {
        this.interestRegion = interestRegion;

        // get ESA satellite temperature data for region and calculate current average value
        LandSurfaceTemperatureManagerSingleton.INSTANCE.temperatureWithinRegion(interestRegion, new OnSuccessCallback() {
            @Override
            public void onSuccess(Object result) {
                List<GeoValue> tempData = (List<GeoValue>) result;
                initialRegionAverageValue = calculateAverageValue(tempData);
                events.clear();
            }
        }, null);

    }


    /**
     * Run the detector model for the region it was prepared for.
     */
    public void run() {
        // prepare if not already done
        if (Float.isNaN(initialRegionAverageValue)) {
            prepare(interestRegion);
            return;
        }

        // forward to next timestep
        if (DEBUG_RUNS) {
            timestep.setTime(timestep.getTime() + DEBUG_TIMESTEP_MSEC);
        } else {
            timestep.setTime(Calendar.getInstance().getTimeInMillis());
        }

        // set a buffer range with the timestep for retrieving the data
        Calendar afterDate = Calendar.getInstance();
        afterDate.setTimeInMillis(timestep.getTime() - DATA_RETRIEVAL_TIMESTEP_RANGE_MSEC / 2);
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.setTimeInMillis(timestep.getTime() + DATA_RETRIEVAL_TIMESTEP_RANGE_MSEC / 2);

        if (D) {
            String msg = String.format("Processing timestep %d (%s) for location %.6f, %.6f", timestep.getTime(), timestep.toString(), interestRegion.getLatitude(), interestRegion.getLongitude());
            Log.d(TAG, msg);
        }

        // retrieve data for current timestep and process it
        LandSurfaceTemperatureManagerSingleton.INSTANCE.temperatureWithinRegionAt(
                interestRegion, afterDate, beforeDate,
                new OnSuccessCallback() {

                    @Override
                    public void onSuccess(Object result) {

                        List<GeoValue> tempData = (List<GeoValue>) result;
                        if (D) {
                            Log.d(TAG, "Number of data values received: " + tempData.size());
                        }

                        // find locations with increased temp (threshold degrees above average?)
                        List<GeoValue> hotspots = detectHotspots(tempData);
                        if (D) {
                            Log.d(TAG, "Number of hotspots detected: " + hotspots.size());
                        }

                        // remove hotspots from events that are no longer there
                        for (HazardEvent event : events) {
                            event.decayHotspots(hotspots);
                        }

                        // go through list of hotspots
                        for (GeoValue hotspot : hotspots) {
                            boolean newHotspot = true;

                            // see if hotspot already part of an event
                            for (HazardEvent event : events) {
                                // hotspot already tracked as part of an event
                                if (event.containesHotspot(hotspot)) {
                                    if (D) {
                                        Log.d(TAG, "Hotspot is already part of a hazard event, updating");
                                    }
                                    event.updateHotspotValue(hotspot);
                                    newHotspot = false;
                                    break;
                                }
                                // hotspot is near an existing event
                                if (event.isNearHotspot(hotspot)) {
                                    if (D) {
                                        Log.d(TAG, "Hotspot is near a hazard event, including");
                                    }
                                    event.addHotspot(hotspot);
                                    newHotspot = false;
                                    break;
                                }
                            }

                            // create a new event for a yet undetected hotspot
                            if (newHotspot) {
                                if (D) {
                                    Log.d(TAG, "Hotspot is new, adding hazard event");
                                }
                                HazardEvent newEvent = HazardEvent.create("New Event for " + interestRegion.getLabel());
                                newEvent.addHotspot(hotspot);
                                events.add(newEvent);
                            }
                        }

                        // remove events that no longer have hotspots
                        Iterator<HazardEvent> iter = events.iterator();
                        while (iter.hasNext()) {
                            HazardEvent event = iter.next();
                            if (!event.hasHotspots()) {
                                if (D) {
                                    Log.d(TAG, "Cancelling hazard event for " + interestRegion.getLabel());
                                }
                                iter.remove();
                            }
                        }
                    }

                }, null);

    }


    public void cleanUp() {
        this.interestRegion = null;
        events.clear();
    }


    public String report() {
        String label = interestRegion.getLabel();

        int totalHotspots = 0;
        for (HazardEvent he : events) {
            totalHotspots += he.hotspotsCount();
        }

        return String.format("For region %s tracking %d events with %d hotspots in total", label, events.size(), totalHotspots);
    }


    private List<GeoValue> detectHotspots(List<GeoValue> data) {
        List<GeoValue> hotspots = new ArrayList<GeoValue>();
        for (GeoValue geoValue : data) {
            if (geoValue.getValue() >= initialRegionAverageValue + HOTSPOT_TEMP_DIFF_THRESHOLD_DEGREES) {
                hotspots.add(geoValue);
            }
        }

        return hotspots;
    }


    private float calculateAverageValue(List<GeoValue> data) {
        float sum = 0;
        int numOfValues = data.size();
        for (GeoValue geoValue : data) {
            sum += geoValue.getValue();
        }

        return sum / numOfValues;
    }


    public InterestRegion getInterestRegion() {
        return interestRegion;
    }


    public List<HazardEvent> getHazardEvents() {
        return events;
    }
}
