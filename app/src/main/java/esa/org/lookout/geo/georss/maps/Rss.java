package esa.org.lookout.geo.georss.maps;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

public class Rss {

    private Channel channel;
    private String version;
    private String xmlnsAtom;
    private String xmlnsGeorss;
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public Channel getChannel() {
        return channel;
    }


    public void setChannel(Channel channel) {
        this.channel = channel;
    }


    public String getVersion() {
        return version;
    }


    public void setVersion(String version) {
        this.version = version;
    }


    public String getXmlnsAtom() {
        return xmlnsAtom;
    }


    public void setXmlnsAtom(String xmlnsAtom) {
        this.xmlnsAtom = xmlnsAtom;
    }


    public String getXmlnsGeorss() {
        return xmlnsGeorss;
    }


    public void setXmlnsGeorss(String xmlnsGeorss) {
        this.xmlnsGeorss = xmlnsGeorss;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
