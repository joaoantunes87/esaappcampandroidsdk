package esa.org.lookout.geo.georss;

import android.util.Log;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import esa.org.lookout.geo.georss.rush.Feeds;

/**
 * Created by Daniel on 2014-09-10 11:41.
 */
public class GeoRSSpiceRequest extends GoogleHttpClientSpiceRequest<Feeds> {
    private final String feedsUrl;


    public GeoRSSpiceRequest(String feedsUrl) {
        super(Feeds.class);
        this.feedsUrl = feedsUrl;
    }


    @Override
    public Feeds loadDataFromNetwork() throws Exception {
        GeoRSSRushDataHandler rh = null;
        try {
            HttpRequest request = getHttpRequestFactory().buildGetRequest(
                    new GenericUrl(feedsUrl));
            HttpResponse response = request.execute();

            InputStream in;
            in = response.getContent();

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            rh = new GeoRSSRushDataHandler();

            xr.setContentHandler(rh);
            InputSource insour = new InputSource(in);
            xr.parse(insour);
        } catch (SAXException e) {
            Log.e("GeoRSS Handler SAX Error", e.toString());
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            Log.e("GeoRSS Handler Parser Config", e.toString());
        }
        //noinspection ConstantConditions
        return rh.getFeeds();
    }
}
