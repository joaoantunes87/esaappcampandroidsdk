package esa.org.lookout.geo.georss;


import org.apache.commons.lang.ArrayUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import esa.org.lookout.geo.georss.rush.Channel;
import esa.org.lookout.geo.georss.rush.Feeds;
import esa.org.lookout.geo.georss.rush.Guid;
import esa.org.lookout.geo.georss.rush.Image;
import esa.org.lookout.geo.georss.rush.Item;
import esa.org.lookout.geo.georss.rush.Link;
import esa.org.lookout.geo.georss.rush.OrigLink;
import esa.org.lookout.geo.georss.rush.Point;
import esa.org.lookout.geo.georss.rush.Rss;
import esa.org.lookout.geo.georss.rush.Source;

/**
 * Created by Daniel on 2014-09-09.
 */

class GeoRSSRushDataHandler extends DefaultHandler {
    private static String FILTER = "Forest fire, wild fire";
    private final Feeds feeds;
    private StringBuffer chars = new StringBuffer();
    private Rss rss;
    private Channel channel;
    private Link atomLink;
    private List<Link> atomLinks;
    private List<String> categoryList;
    private Image image;
    private boolean isInImage;
    private Item item;
    private final List<Item> itemList = new ArrayList<Item>();
    private boolean isInItem;
    private Guid guid;
    private Source source;
    private Point point;
    private OrigLink origlink;


    public GeoRSSRushDataHandler() {
        super();
        feeds = new Feeds();
    }


    public Feeds getFeeds() {
        return feeds;
    }


    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes atts) throws SAXException {
        super.startElement(uri, localName, qName, atts);

        chars = new StringBuffer();
        //init GEORSS object
        if (localName.equalsIgnoreCase("rss")) {
            rss = new Rss();

            //init channel object
        } else if (localName.equalsIgnoreCase("channel")) {
            channel = new Channel();
        } else if (localName.equalsIgnoreCase("link")) {
            atomLink = new Link();
            atomLink.setXmlns(atts.getValue("xmlns"));
            atomLink.setRel(atts.getValue("rel"));
            atomLink.setType(atts.getValue("type"));
            atomLink.setHref(atts.getValue("href"));
            atomLinks = new ArrayList<Link>();
        } else if (localName.equalsIgnoreCase("category")) {
            categoryList = new ArrayList<String>();
        } else if (localName.equalsIgnoreCase("image")) {
            image = new Image();
            isInImage = true;
        } else if (localName.equalsIgnoreCase("item")) {
            item = new Item();
            isInItem = true;
        } else if (localName.equalsIgnoreCase("guid")) {
            guid = new Guid();
            guid.setIsPermaLink(atts.getValue("isPermaLink"));
        } else if (localName.equalsIgnoreCase("source")) {
            source = new Source();
            source.setUrl(atts.getValue("url"));
        } else if (localName.equalsIgnoreCase("point")) {
            point = new Point();
        } else if (localName.equalsIgnoreCase("origlink")) {
            origlink = new OrigLink();
        }

    }


    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        super.endElement(uri, localName, qName);
        if (localName.equalsIgnoreCase("rss")) {
            feeds.setRss(rss);
        } else if (localName.equalsIgnoreCase("channel")) {
            channel.setAtomLinks(atomLinks);
            channel.setCategory(categoryList);
            channel.setItem(itemList);
            rss.setChannel(channel);
        } else if (localName.equalsIgnoreCase("title")) {
            channel.setTitle(chars.toString());
        } else if (localName.equalsIgnoreCase("description")) {
            channel.setDescription(chars.toString());
        } else if (localName.equalsIgnoreCase("link")) {
            channel.setChannelLink(chars.toString());
        } else if (localName.equalsIgnoreCase("link")) {
            atomLinks.add(atomLink);
        } else if (localName.equalsIgnoreCase("language")) {
            channel.setLanguage(chars.toString());
        } else if (localName.equalsIgnoreCase("category")) {
            categoryList.add(chars.toString());
        } else if (localName.equalsIgnoreCase("image")) {
            channel.setImage(image);
            isInImage = false;
        } else if (localName.equalsIgnoreCase("copyright")) {
            channel.setCopyright(chars.toString());
        } else if (localName.equalsIgnoreCase("managingeditor")) {
            channel.setManagingEditor(chars.toString());
        } else if (localName.equalsIgnoreCase("webmaster")) {
            channel.setWebMaster(chars.toString());
        } else if (localName.equalsIgnoreCase("generator")) {
            channel.setGenerator(chars.toString());
        } else if (localName.equalsIgnoreCase("pubdate")) {
            channel.setPubDate(chars.toString());
        } else if (localName.equalsIgnoreCase("lastbuilddate")) {
            channel.setLastBuildDate(chars.toString());
        } else if (localName.equalsIgnoreCase("item")) {
            filterItems();
            isInItem = false;
        } else if (localName.equalsIgnoreCase("title")) {
            channel.setTitle(chars.toString());
        }
        //image (thumbnails) entry
        if (isInImage) {
            if (localName.equalsIgnoreCase("link")) {
                image.setLink(chars.toString());
            } else if (localName.equalsIgnoreCase("url")) {
                image.setUrl(chars.toString());
            } else if (localName.equalsIgnoreCase("title")) {
                image.setTitle(chars.toString());
            }
        }
        // each item parser
        if (isInItem) {
            if (localName.equalsIgnoreCase("title")) {
                item.setTitle(formatTitle(chars.toString()));
            } else if (localName.equalsIgnoreCase("link")) {
                item.setLink(chars.toString());
            } else if (localName.equalsIgnoreCase("description")) {
                item.setDescription(formatDescription(chars.toString()));
            } else if (localName.equalsIgnoreCase("category")) {
                item.setCategory(chars.toString());
            } else if (localName.equalsIgnoreCase("guid")) {
                guid.setText(chars.toString());
                item.setGuid(guid);
            } else if (localName.equalsIgnoreCase("pubdate")) {
                item.setPubDate(chars.toString());
            } else if (localName.equalsIgnoreCase("source")) {
                source.setText(chars.toString());
                item.setSource(source);
            } else if (localName.equalsIgnoreCase("point")) {
                point.setText(chars.toString());
                item.setPoint(point);
            } else if (localName.equalsIgnoreCase("origlink")) {
                origlink.setText(chars.toString());
                item.setOrigLink(origlink);
            }
        }
    }


    private String formatTitle(String s) {
        s = s.replaceAll("\\[.*?\\]", "");
        return s;
    }


    private String formatDescription(String html) {
        String cleanDesc = "";
        html = html.replaceAll("\\&lt;.*?\\&gt;", "");
        html = html.replaceAll("\\<.*?\\>", "");
        String[] descParts = html.split("\n");
        ArrayUtils.reverse(descParts);
        for (String str : descParts) {
            cleanDesc = cleanDesc + str + "\n";
        }
        return cleanDesc;
    }


    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        chars.append(new String(ch, start, length));
        super.characters(ch, start, length);
    }


    private void filterItems() {
        if (item.getCategory().equalsIgnoreCase(FILTER)) {
            itemList.add(item);
        }
    }

}
