package esa.org.lookout.geo;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class GeofenceUtils {

    private final static String TAG = GeofenceUtils.class.getSimpleName();
    private final static boolean D = true;


    public enum REMOVE_TYPE {INTENT, LIST}


    public enum REQUEST_TYPE {ADD, REMOVE}


    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final CharSequence GEOFENCE_ID_DELIMITER = ",";


    public static boolean servicesConnected(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode == ConnectionResult.SUCCESS) {
            if (D) {
                Log.d(TAG, "Google Play Services is available");
            }
            return true;
        } else {
            Log.e(TAG, "Google Play Services are not available!");
            return false;
        }
    }

}
