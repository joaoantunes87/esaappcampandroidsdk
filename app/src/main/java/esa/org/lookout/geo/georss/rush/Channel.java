package esa.org.lookout.geo.georss.rush;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Channel {

    private String title;
    private String description;
    private String channelLink;
    private List<Link> atomLinks = new ArrayList<Link>();
    private String language;
    private List<String> category = new ArrayList<String>();
    private Image image;
    private String copyright;
    private String managingEditor;
    private String webMaster;
    private String generator;
    private String pubDate;
    private String lastBuildDate;
    private Info info;
    private List<Item> item = new ArrayList<Item>();
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getChannelLink() {
        return channelLink;
    }


    public void setChannelLink(String channelLink) {
        this.channelLink = channelLink;
    }


    public List<Link> getAtomLinks() {
        return atomLinks;
    }


    public void setAtomLinks(List<Link> atomLinks) {
        this.atomLinks = atomLinks;
    }


    public String getLanguage() {
        return language;
    }


    public void setLanguage(String language) {
        this.language = language;
    }


    public List<String> getCategory() {
        return category;
    }


    public void setCategory(List<String> category) {
        this.category = category;
    }


    public Image getImage() {
        return image;
    }


    public void setImage(Image image) {
        this.image = image;
    }


    public String getCopyright() {
        return copyright;
    }


    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }


    public String getManagingEditor() {
        return managingEditor;
    }


    public void setManagingEditor(String managingEditor) {
        this.managingEditor = managingEditor;
    }


    public String getWebMaster() {
        return webMaster;
    }


    public void setWebMaster(String webMaster) {
        this.webMaster = webMaster;
    }


    public String getGenerator() {
        return generator;
    }


    public void setGenerator(String generator) {
        this.generator = generator;
    }


    public String getPubDate() {
        return pubDate;
    }


    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }


    public String getLastBuildDate() {
        return lastBuildDate;
    }


    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }


    public Info getInfo() {
        return info;
    }


    public void setInfo(Info info) {
        this.info = info;
    }


    public List<Item> getItem() {
        return item;
    }


    public void setItem(List<Item> item) {
        this.item = item;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
