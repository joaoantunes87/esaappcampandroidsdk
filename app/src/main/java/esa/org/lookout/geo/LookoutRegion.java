package esa.org.lookout.geo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.location.Geofence;

import java.util.UUID;

/**
 * A region that the app is tracking alert status for.
 * <p/>
 * Lookout regions currently are circular, since this is the only type Google
 * GeoFence functionality supports at the moment.
 * <p/>
 * Created by Rob Knapen on 09/09/14.
 */
public class LookoutRegion implements Parcelable {

    public static final float DEFAULT_RADIUS = 50;

    // Parameters used when creating a default Google Geofence for the region
    // TODO tune params for real usage of the app
    public static long GEOFENCE_EXPIRATION_DURATION_MSEC = Geofence.NEVER_EXPIRE;
    public static int GEOFENCE_NOTIFICATION_RESPONSIVENESS_MSEC = 1 * 60 * 1000; // 1 minute

    // green: no risk in this zone
    public static final int ALERT_STATUS_GREEN = 0;
    // yellow: potential risk, still needs to be validate by authorities
    public static final int ALERT_STATUS_YELLOW = 1;
    // red: real validated risk
    public static final int ALERT_STATUS_RED = 2;

    // always need on undefined status :)
    public static int RISK_TYPE_UNDEFINED = 0;
    // initial type of risk the app will be tracking
    public static int RISK_TYPE_LARGE_FIRE = 1;

    // lookout zone information
    private String label = "New Lookout Region";
    private String description = "";
    private int alertStatus = ALERT_STATUS_YELLOW;
    private int riskType = RISK_TYPE_LARGE_FIRE;

    // circular region location
    private double latitude;
    private double longitude;
    private float radius;


    public static LookoutRegion createRegion(String label, double latitude, double longitude, float radius) {
        LookoutRegion region = new LookoutRegion();
        region.setLabel(label);
        region.setAlertStatus(ALERT_STATUS_YELLOW);
        region.setRiskType(RISK_TYPE_UNDEFINED);
        region.setLatitude(latitude);
        region.setLongitude(longitude);
        region.setRadius(radius);
        return region;
    }


    public static LookoutRegion fromInterestRegion(InterestRegion region) {

        if (region == null) {
            throw new IllegalArgumentException("region is required!");
        }

        LookoutRegion lookoutRegion = createRegion(
                region.getLabel(),
                region.getLatitude(),
                region.getLongitude(),
                region.getRadius()
        );

        lookoutRegion.setDescription(region.getDescription());

        return lookoutRegion;
    }


    public LookoutRegion() {
    }


    public LookoutRegion(String label, String description, int alertStatus, int riskType, double latitude, double longitude, float radius) {
        this.label = label;
        this.description = description;
        this.alertStatus = alertStatus;
        this.riskType = riskType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(description);
        dest.writeInt(alertStatus);
        dest.writeInt(riskType);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeFloat(radius);
    }


    public LookoutRegion(Parcel in) {
        label = in.readString();
        description = in.readString();
        alertStatus = in.readInt();
        riskType = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        radius = in.readFloat();
    }


    public static final Parcelable.Creator<LookoutRegion> CREATOR = new Parcelable.Creator<LookoutRegion>() {
        public LookoutRegion createFromParcel(Parcel in) {
            return new LookoutRegion(in);
        }


        public LookoutRegion[] newArray(int size) {
            return new LookoutRegion[size];
        }
    };


    @Override
    public String toString() {
        return "LookoutRegion{" +
                "label='" + label + '\'' +
                ", description='" + description + '\'' +
                ", alertStatus=" + alertStatus +
                ", riskType=" + riskType +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                '}';
    }


    private boolean coordEqual(double val1, double val2) {
        return (Math.abs(val1 - val2) < 0.00001);
    }


    private boolean radiusEqual(float val1, float val2) {
        return (Math.abs(val1 - val2) < 0.1);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LookoutRegion)) return false;

        LookoutRegion that = (LookoutRegion) o;

        if (!coordEqual(that.latitude, latitude)) return false;
        if (!coordEqual(that.longitude, longitude)) return false;
        if (!radiusEqual(that.radius, radius)) return false;
        if (riskType != that.riskType) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result;
        long temp;
        result = riskType;
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (radius != +0.0f ? Float.floatToIntBits(radius) : 0);
        return result;
    }


    public Geofence toGeofence() {
        return toGeofence(UUID.randomUUID().toString(),
                GEOFENCE_EXPIRATION_DURATION_MSEC,
                GEOFENCE_NOTIFICATION_RESPONSIVENESS_MSEC);
    }


    public Geofence toGeofence(String id, long expirationDurationMSec, int notificationResponsivenessMSec) {
        return new Geofence.Builder()
                .setCircularRegion(getLatitude(), getLongitude(), getRadius())
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .setExpirationDuration(expirationDurationMSec)
                .setNotificationResponsiveness(notificationResponsivenessMSec)
                .setRequestId(id)
                .build();
    }


    public String getLabel() {
        return label;
    }


    public void setLabel(String label) {
        this.label = label;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public int getAlertStatus() {
        return alertStatus;
    }


    public void setAlertStatus(int alertStatus) {
        this.alertStatus = alertStatus;
    }


    public int getRiskType() {
        return riskType;
    }


    public void setRiskType(int riskType) {
        this.riskType = riskType;
    }


    public double getLatitude() {
        return latitude;
    }


    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public double getLongitude() {
        return longitude;
    }


    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public float getRadius() {
        return radius;
    }


    public void setRadius(float radius) {
        this.radius = radius;
    }
}
