package esa.org.lookout.exceptions;

/**
 * Created by joaoantunes on 11/09/2014.
 */
public class NoResultsException extends Exception {

    public NoResultsException() {
    }


    public NoResultsException(String detailMessage) {
        super(detailMessage);
    }


    public NoResultsException(Throwable throwable) {
        super(throwable);
    }


    public NoResultsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

}
