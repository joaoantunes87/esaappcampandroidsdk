package esa.org.lookout.backend;

import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import esa.org.lookout.geo.InterestRegion;

/**
 * Created by joaoantunes on 10/09/2014.
 */
public class InterestRegionFactory {

    public final static String INTEREST_REGION_TABLE_KEY = "InterestRegion";

    public final static String LOCATION_KEY = "LOCATION";
    public final static String RADIUS_KEY = "RADIUS";
    public final static String LABEL_KEY = "LABEL";
    public final static String DESCRIPTION_KEY = "DESCRIPTION";


    public static ParseObject createInterestRegionForParse(InterestRegion interestRegion) {

        if (interestRegion == null) {
            throw new IllegalArgumentException("interestRegion is required!");
        }

        ParseObject interestRegionParseObject = ParseObject.create(INTEREST_REGION_TABLE_KEY);
        ParseGeoPoint geoPoint = new ParseGeoPoint(interestRegion.getLatitude(), interestRegion.getLongitude());
        interestRegionParseObject.put(LOCATION_KEY, geoPoint);
        interestRegionParseObject.put(RADIUS_KEY, interestRegion.getRadius());
        interestRegionParseObject.put(LABEL_KEY, interestRegion.getLabel());
        interestRegionParseObject.put(DESCRIPTION_KEY, interestRegion.getDescription());

        return interestRegionParseObject;

    }

}
