package esa.org.lookout.backend;

import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import esa.org.lookout.geo.LookoutRegion;

/**
 * Created by joaoantunes on 12/09/2014.
 */
public class LookoutRegionFactory {

    public final static String LOOKOUT_REGION_TABLE_KEY = "LookoutRegion";

    public final static String LOCATION_KEY = "LOCATION";
    public final static String RADIUS_KEY = "RADIUS";
    public final static String LABEL_KEY = "LABEL";
    public final static String DESCRIPTION_KEY = "DESCRIPTION";
    public final static String ALERT_STATUS_KEY = "ALERT_STATUS";
    public final static String RISK_TYPE = "RISK_TYPE";


    public static ParseObject createLookoutRegionForParse(LookoutRegion lookoutRegion) {

        if (lookoutRegion == null) {
            throw new IllegalArgumentException("lookoutRegion is required!");
        }

        ParseObject lookoutRegionParseObject = ParseObject.create(LOOKOUT_REGION_TABLE_KEY);
        ParseGeoPoint geoPoint = new ParseGeoPoint(lookoutRegion.getLatitude(), lookoutRegion.getLongitude());
        lookoutRegionParseObject.put(LOCATION_KEY, geoPoint);
        lookoutRegionParseObject.put(ALERT_STATUS_KEY, lookoutRegion.getAlertStatus());
        lookoutRegionParseObject.put(RISK_TYPE, lookoutRegion.getRiskType());
        lookoutRegionParseObject.put(RADIUS_KEY, lookoutRegion.getRadius());
        lookoutRegionParseObject.put(LABEL_KEY, lookoutRegion.getLabel());
        lookoutRegionParseObject.put(DESCRIPTION_KEY, lookoutRegion.getDescription());

        return lookoutRegionParseObject;

    }

}
