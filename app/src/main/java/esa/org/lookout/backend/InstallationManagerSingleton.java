package esa.org.lookout.backend;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import esa.org.lookout.geo.InterestRegion;
import esa.org.lookout.geo.LookoutRegion;

/**
 * Created by joaoantunes on 10/09/2014.
 */
public enum InstallationManagerSingleton {
    INSTANCE;

    /**
     * CONSTANTS *
     */
    private static final String LAST_LOCATION_KEY = "LAST_LOCATION";
    private static final String INTERESTED_REGIONS_KEY = "INTERESTED_REGION";
    private static final String USER_TYPE_KEY = "USER_TYPE_KEY";

    /**
     * Instance Attributes
     */
    private ParseInstallation currentInstallation;


    private InstallationManagerSingleton() {
        this.refreshInstallation();
    }


    public void saveCurrentInstallationLocation(double latitude, double longitude) {
        ParseGeoPoint pointCoordinates = new ParseGeoPoint(latitude, longitude);
        this.currentInstallation.put(LAST_LOCATION_KEY, pointCoordinates);
        this.currentInstallation.saveInBackground();
    }


    public void addInterestRegion(InterestRegion interestRegion) {

        if (interestRegion == null) {
            throw new IllegalArgumentException("interestRegion is required!");
        }

        final ParseRelation interestedRegionsRelations = this.currentInstallation.getRelation(INTERESTED_REGIONS_KEY);
        final ParseObject interestRegionParseObject = InterestRegionFactory.createInterestRegionForParse(interestRegion);
        interestRegionParseObject.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException parseException) {

                if (parseException == null) {
                    interestedRegionsRelations.add(interestRegionParseObject);
                    currentInstallation.saveInBackground();
                }

            }

        });

    }


    public void deleteInterestRegion(InterestRegion interestRegion) {
        // TODO
    }


    public void refreshInstallation() {
        this.currentInstallation = ParseInstallation.getCurrentInstallation();

        // FIXME to remove. In the future there will ber a better way to define the user profiles
        this.currentInstallation.put(USER_TYPE_KEY, UserProfile.EMERGENCY_RESCUE_PROFESSIONAL.toString());
        this.currentInstallation.saveInBackground();
    }


    public void sendPushNotificationToProfessionalAt(LookoutRegion region) {
        sendPushNotificationToUsersAt(region, UserProfile.EMERGENCY_RESCUE_PROFESSIONAL);
    }


    public void sendPushNotificationToOrdinaryUsersAt(LookoutRegion region) {
        sendPushNotificationToUsersAt(region, UserProfile.ORDINARY_USER);
    }


    private void sendPushNotificationToUsersAt(LookoutRegion region, UserProfile userProfile) {

        if (region != null) {

            ParseQuery<ParseInstallation> nearUsers = ParseInstallation.getQuery();
            ParseGeoPoint parseGeoPoint = new ParseGeoPoint(region.getLatitude(), region.getLongitude());

            // warn users near to the region
            nearUsers.whereWithinKilometers(LAST_LOCATION_KEY, parseGeoPoint, region.getRadius());

            if (userProfile != null) {
                nearUsers.whereEqualTo(USER_TYPE_KEY, userProfile.toString());
            }

            sendPushNotificationsToInstallations(nearUsers, "Fire near to you!", region);

            // warn users looking out region
            final ParseQuery<ParseInstallation> regionWatchUsers = ParseInstallation.getQuery();
            ParseQuery affectedRegions = ParseQuery.getQuery(InterestRegionFactory.INTEREST_REGION_TABLE_KEY);
            affectedRegions.whereWithinKilometers(InterestRegionFactory.LOCATION_KEY, new ParseGeoPoint(region.getLatitude(), region.getLongitude()), region.getRadius());
            regionWatchUsers.whereMatchesQuery(INTERESTED_REGIONS_KEY, affectedRegions);

            if (userProfile != null) {
                regionWatchUsers.whereEqualTo(USER_TYPE_KEY, userProfile.toString());
            }

            sendPushNotificationsToInstallations(regionWatchUsers, "Fire in one of your interest regions!", region);

        } else {
            throw new IllegalArgumentException("region parameter is required!");
        }

    }


    private void sendPushNotificationsToInstallations(ParseQuery<ParseInstallation> installationsQuery, String message, LookoutRegion region) {

        ParsePush pushNotification = new ParsePush();
        pushNotification.setQuery(installationsQuery);

        try {
            JSONObject notificationData = new JSONObject();
            notificationData.put("alert", message);
            notificationData.put("region", region);
            pushNotification.setData(notificationData);
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }

        pushNotification.sendInBackground();

    }

}
