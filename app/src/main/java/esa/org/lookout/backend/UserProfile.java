package esa.org.lookout.backend;

/**
 * Created by joaoantunes on 13/09/2014.
 */
public enum UserProfile {
    ORDINARY_USER,
    EMERGENCY_RESCUE_PROFESSIONAL;
}
