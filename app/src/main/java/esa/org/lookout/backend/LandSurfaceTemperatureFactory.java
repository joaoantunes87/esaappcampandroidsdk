package esa.org.lookout.backend;

import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import esa.org.lookout.geo.detector.GeoValue;
import esa.org.lookout.utils.DateUtils;

/**
 * Created by joaoantunes on 11/09/2014.
 */
public class LandSurfaceTemperatureFactory {

    public static final String LAND_SURFACE_TEMPERATURE_TABLE_KEY = "LandSurfaceTemperature";

    public static final String GEO_POINT_KEY = "GeoPoint";
    public static final String VALUE_KEY = "Value";
    public static final String TIMESTAMP_KEY = "Timestamp";


    public static ParseObject createLandSurfaceTemperatureToParseFromGeoValue(
            GeoValue geoValue) {

        ParseObject temperatureRow = ParseObject.create(LAND_SURFACE_TEMPERATURE_TABLE_KEY);
        ParseGeoPoint geoPoint = new ParseGeoPoint(geoValue.getLatitude(), geoValue.getLongitude());
        temperatureRow.put(GEO_POINT_KEY, geoPoint);
        temperatureRow.put(VALUE_KEY, geoValue.getValue());
        temperatureRow.put(TIMESTAMP_KEY, DateUtils.convertCalendarToUnixTimestamp(geoValue.getDate()));

        return temperatureRow;

    }


    public static GeoValue createGeoValueFromParseObject(ParseObject parseObject) {

        if (parseObject == null) {
            throw new IllegalArgumentException("parseObject is required!");
        }

        ParseGeoPoint geoPoint = parseObject.getParseGeoPoint(GEO_POINT_KEY);
        GeoValue geoValue = new GeoValue();
        geoValue.setLatitude(geoPoint.getLatitude());
        geoValue.setLongitude(geoPoint.getLongitude());
        geoValue.setValue(new Double(parseObject.getDouble(VALUE_KEY)).floatValue());
        geoValue.setDate(DateUtils.convertUnixTimestampToCalendar(parseObject.getLong(TIMESTAMP_KEY)));

        return geoValue;

    }

}
