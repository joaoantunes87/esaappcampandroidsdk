package esa.org.lookout.backend;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import esa.org.lookout.geo.LookoutRegion;

/**
 * Created by joaoantunes on 12/09/2014.
 */
public enum LookoutRegionManagerSingleton {
    INSTANCE;


    public static void saveLookoutRegion(LookoutRegion lookoutRegion, final OnSuccessCallback onSuccessCallback, OnFailCallback onFailCallback) {

        if (lookoutRegion == null) {
            throw new IllegalArgumentException("lookoutRegion is required!");
        }

        final ParseObject lookoutRegionParse = LookoutRegionFactory.createLookoutRegionForParse(lookoutRegion);
        lookoutRegionParse.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException parseException) {

                if (parseException == null) {
                    if (onSuccessCallback != null) {
                        onSuccessCallback.onSuccess(lookoutRegionParse);
                    } else {
                        // TODO on fail
                    }
                }
            }

        });

    }

}
