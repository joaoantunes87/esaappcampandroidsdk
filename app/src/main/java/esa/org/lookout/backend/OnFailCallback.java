package esa.org.lookout.backend;

/**
 * Created by joaoantunes on 10/09/2014.
 */
public interface OnFailCallback {
    public void onFail(Error error);
}
