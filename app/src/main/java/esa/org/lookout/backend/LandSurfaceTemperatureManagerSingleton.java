package esa.org.lookout.backend;

import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.ujuizi.ramani.RSMapServices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import esa.org.lookout.exceptions.NoResultsException;
import esa.org.lookout.geo.InterestRegion;
import esa.org.lookout.geo.detector.GeoValue;
import esa.org.lookout.utils.DateUtils;

/**
 * Created by joaoantunes on 10/09/2014.
 */
public enum LandSurfaceTemperatureManagerSingleton {
    INSTANCE;

    private static final String LAYER_ID = "simS3seriesLstMaxSicilyIT/tmax_rec";


    public Double temperatureAt(double latitude, double longitude) throws NoResultsException {

        LatLng pointCoordinates = new LatLng(latitude, longitude);
        String temperature = RSMapServices.getFeatureInfo(pointCoordinates, LAYER_ID);

        if (temperature != null) {
            try {
                return Double.valueOf(temperature);
            } catch (NumberFormatException numberFormatException) {
                // an exception will be thrown
                // FIXME to remove -- just generating dummy data
                Random random = new Random(System.currentTimeMillis());
                double rangeMin = -10;
                double rangeMax = 100;
                double randomTemperatureValue = rangeMin + (rangeMax - rangeMin) * random.nextDouble();
                return randomTemperatureValue;
            }
        }

        throw new NoResultsException("No temperature value was fetched.");

    }


    public void saveTemperatureAtPointWithCallbacks(final double latitude,
                                                    final double longitude,
                                                    final OnSuccessCallback successCallback,
                                                    final OnFailCallback onFailCallback) throws NoResultsException {

        Double temperature = temperatureAt(latitude, longitude);

        GeoValue geoValue = new GeoValue();
        geoValue.setLatitude(latitude);
        geoValue.setLongitude(longitude);
        geoValue.setValue(temperature.floatValue());
        geoValue.setDate(Calendar.getInstance());

        ParseObject temperatureRow = LandSurfaceTemperatureFactory.createLandSurfaceTemperatureToParseFromGeoValue(geoValue);
        temperatureRow.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException parseException) {

                if (parseException == null && successCallback != null) {
                    successCallback.onSuccess(null);
                } else {
                    // TODO
                }
            }
        });

    }


    public void saveTemperatureAt(double latitude, double longitude) throws NoResultsException {
        saveTemperatureAtPointWithCallbacks(latitude, longitude, null, null);
    }


    public void temperatureWithinRegion(InterestRegion region, OnSuccessCallback onSuccessCallback, OnFailCallback onFailCallback) {
        temperatureWithinRegionAt(region, null, null, onSuccessCallback, onFailCallback);
    }


    public void temperatureWithinRegionAt(InterestRegion region, Calendar afterDate, Calendar beforeDate, final OnSuccessCallback onSuccessCallback, OnFailCallback onFailCallback) {

        if (region == null) {
            throw new IllegalArgumentException("region is required!");
        }

        ParseQuery temperatureQuery = ParseQuery.getQuery(LandSurfaceTemperatureFactory.LAND_SURFACE_TEMPERATURE_TABLE_KEY);
        temperatureQuery.whereWithinKilometers(LandSurfaceTemperatureFactory.GEO_POINT_KEY,
                new ParseGeoPoint(region.getLatitude(), region.getLongitude()),
                region.getRadius()
        );

        if (afterDate != null) {
            temperatureQuery.whereGreaterThanOrEqualTo(LandSurfaceTemperatureFactory.TIMESTAMP_KEY, DateUtils.convertCalendarToUnixTimestamp(afterDate));
        }

        if (beforeDate != null) {
            temperatureQuery.whereLessThanOrEqualTo(LandSurfaceTemperatureFactory.TIMESTAMP_KEY, DateUtils.convertCalendarToUnixTimestamp(beforeDate));
        }

        temperatureQuery.orderByDescending(LandSurfaceTemperatureFactory.TIMESTAMP_KEY);
        temperatureQuery.findInBackground(new FindCallback() {

            @Override
            public void done(List list, ParseException parseException) {

                if (parseException == null && list != null) {

                    if (onSuccessCallback != null) {

                        List<GeoValue> landSurfaceTemperatures = new ArrayList<GeoValue>(list.size());
                        for (Object item : list) {
                            if (item instanceof ParseObject) {
                                ParseObject parseObject = (ParseObject) item;
                                GeoValue geoLandSurfaceTemperature = LandSurfaceTemperatureFactory.createGeoValueFromParseObject(parseObject);
                                landSurfaceTemperatures.add(geoLandSurfaceTemperature);
                            }
                        }

                        onSuccessCallback.onSuccess(landSurfaceTemperatures);

                    }

                } else {

                }

            }

        });

    }

}
