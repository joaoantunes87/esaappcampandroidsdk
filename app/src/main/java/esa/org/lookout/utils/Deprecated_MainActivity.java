package esa.org.lookout.utils;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import esa.org.lookout.ui.R;

//import com.ujuizi.ramani.RSMapServices;

public class Deprecated_MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = getFragmentManager().findFragmentById(R.id.map);
        MapFragment mapFragment = (MapFragment) fragment;
        GoogleMap map = mapFragment.getMap();
        LatLng mali = new LatLng(37.7533, 14.9290);
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(mali, 13));

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String layerId = "simS3seriesLstMaxSicilyIT/tmax_rec";

        /*TileProvider tp = RSMapServices.getMap(layerId);
        JSONObject metadata = RSMapServices.getMetadata(layerId);
        String metadataJsonString = metadata.toString();
        Log.d("Test", metadataJsonString);

        String featureInfo = RSMapServices.getFeatureInfo(mali, layerId);
        Log.d("Test", featureInfo);
        map.addTileOverlay(new TileOverlayOptions().tileProvider(tp).fadeIn(true));*/

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
