package esa.org.lookout.utils;

import java.util.Calendar;

/**
 * Created by joaoantunes on 11/09/2014.
 */
public class DateUtils {

    public static long convertCalendarToUnixTimestamp(Calendar calendar) {

        if (calendar == null) {
            calendar = Calendar.getInstance();
        }

        return calendar.getTimeInMillis() / 1000;

    }


    public static Calendar convertUnixTimestampToCalendar(long unixTimestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixTimestamp * 1000);

        return calendar;

    }

}
