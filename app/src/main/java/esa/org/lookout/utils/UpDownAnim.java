package esa.org.lookout.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by jonas on 09/09/14.
 */
public class UpDownAnim extends Animation {

    // TODO Improve animation (position is changed before animation starts)

    private final Context c;
    private final View v;
    private final int maxHeight;
    private final boolean down;


    public UpDownAnim(Context c, View v, int maxHeight, boolean down) {
        this.c = c;
        this.v = v;
        this.maxHeight = maxHeight;
        this.down = down;
    }


    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        int height;
        if (down) {
            height = (int) (maxHeight * interpolatedTime);
        } else {
            height = (int) (maxHeight * (1 - interpolatedTime)) + 20;
        }

        float scale = c.getResources().getDisplayMetrics().density;
        int pixels = (int) (height * scale + 0.5f);
        v.getLayoutParams().height = pixels;
        v.requestLayout();
    }


    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }


    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
