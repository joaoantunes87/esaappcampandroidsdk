package esa.org.lookout.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import esa.org.lookout.backend.InstallationManagerSingleton;
import esa.org.lookout.backend.LookoutRegionManagerSingleton;
import esa.org.lookout.backend.OnSuccessCallback;
import esa.org.lookout.geo.InterestRegion;
import esa.org.lookout.geo.LookoutRegion;
import esa.org.lookout.geo.RegionManager;
import esa.org.lookout.geo.detector.HazardDetector;
import esa.org.lookout.geo.detector.HazardEvent;
import esa.org.lookout.geo.georss.rush.Feeds;
import esa.org.lookout.ui.R;
import esa.org.lookout.ui.UiMain;

/**
 * Background service for the Lookout app.
 * <p/>
 * This service will be running the HazardDetector in the background for
 * the InterestRegions.
 * <p/>
 * Created by robknapen on 11/09/14.
 */
public class LookoutService extends Service {

    private static final String TAG = LookoutService.class.getSimpleName();
    private static final boolean D = true;

    private final static int NOTIFICATION_ID = 10001;

    // broadcast intent actions
    public final static String NEW_LOOKOUT_REGION = "esa.org.lookout.new_lookout_region";

    // FIXME: interval at which hazard detectors are run -> set to longer time
    private final static int HAZARD_DETECTORS_RUN_INTERVAL_SEC = 10;

    private PendingIntent notificationPendingIntent;
    private Notification lookoutNotification;
    private String notificationTitle = "Lookout";
    private String notificationContextText = "Service is active";

    // keeps track of the interest and lookout regions
    private RegionManager regionManager;

    // this timer will run the hazard detectors periodically
    private Timer hazardDetectorsTimer;

    // list of hazard detectors for the interest regions
    private List<HazardDetector> hazardDetectors = new ArrayList<HazardDetector>();

    // service binder
    private final IBinder binder = new LookoutServiceBinder();


    public class LookoutServiceBinder extends Binder {
        public LookoutService getService() {
            return LookoutService.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // init a region manager
        regionManager = new RegionManager();

        // create a notification for the lookout service
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationContextText)
                        .setOngoing(true)
                        .setAutoCancel(false);

        Intent ovIntent = new Intent(this, UiMain.class);
        notificationPendingIntent = PendingIntent.getActivity(this, 0, ovIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(notificationPendingIntent);
        lookoutNotification = builder.build();
        setNotificationText("");
        startForeground(NOTIFICATION_ID, lookoutNotification);

        startHazardDetectorsTimer();

        // add sample regions for testing
        addLookoutRegion("Frascati", 41.810604, 12.676506, 5000).setDescription("Sample lookout for Frascati");
        // addInterestRegion("Sample", 41.812043, 12.688437, 5000).setDescription("Hazard Detector sample data region");
        addInterestRegion("Home", 41.810604, 12.676506, 2500).setDescription("Home in Frascati");
        addInterestRegion("Work", 41.901384, 12.485186, 2500).setDescription("Work in Rome");

        return Service.START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        // HACK FOR POPUP
        /*
        try {
            Thread.sleep(15000);
        } catch (InterruptedException ex) {
        }

        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        View view = ((LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_warning_popup, null);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                windowManager.removeView(view);
                return true;
            }
        });

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        windowManager.addView(view, params);
        */
        // ===============
    }


    @Override
    public void onDestroy() {
        stopService();
        super.onDestroy();
    }


    public void stopService() {
        stopGeofenceService();
        stopHazardDetectorsTimer();
        stopSelf();
    }


    public void setNotificationText(String message) {
        if ((lookoutNotification != null) && (notificationPendingIntent != null)) {
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (nm != null) {
                lookoutNotification.setLatestEventInfo(this, notificationTitle, message, notificationPendingIntent);
                lookoutNotification.tickerText = message;
            }

            lookoutNotification.when = System.currentTimeMillis();
            lookoutNotification.number = regionManager.getLookoutRegions().size();
            nm.notify(NOTIFICATION_ID, lookoutNotification);
        }
    }


    public void startGeofenceService(Activity activity) {
        regionManager.startGeofenceService(activity);
    }


    public void stopGeofenceService() {
        regionManager.stopGeofenceService();
    }


    public void addLookoutRegions(Feeds feeds) {
        regionManager.addLookoutRegions(feeds);
        setNotificationText("Latest alerts from Copernicus EMS added");
    }


    public LookoutRegion addLookoutRegion(String label, double latitude, double longitude, float radius) {
        LookoutRegion region = regionManager.addLookoutRegion(label, latitude, longitude, radius);
        setNotificationText("Alert for " + region.getLabel());
        return region;
    }


    public LookoutRegion getLookoutRegionAtIndex(int i) {
        return regionManager.getLookoutRegionAtIndex(i);
    }


    public boolean removeLookoutRegion(LookoutRegion region) {
        boolean result = regionManager.removeLookoutRegion(region);
        setNotificationText("Canceled alert for " + region.getLabel());
        return result;
    }


    public List<LookoutRegion> getLookoutRegions() {
        return regionManager.getLookoutRegions();
    }


    public InterestRegion addInterestRegion(String label, double latitude, double longitude, float radius) {
        InterestRegion ir = regionManager.addInterestRegion(label, latitude, longitude, radius);
        if (findHazardDetector(ir) == null) {
            HazardDetector hd = HazardDetector.create(ir);
            hazardDetectors.add(hd);
        }
        return ir;
    }


    private HazardDetector findHazardDetector(InterestRegion interestRegion) {
        for (HazardDetector hd : hazardDetectors) {
            if (hd.getInterestRegion().equals(interestRegion)) {
                return hd;
            }
        }
        return null;
    }


    public InterestRegion addSpecialInterestRegion(String label, double latitude, double longitude, float radius) {
        return regionManager.addSpecialInterestRegion(label, latitude, longitude, radius);
    }


    public InterestRegion getInterestRegionAtIndex(int i) {
        return regionManager.getInterestRegionAtIndex(i);
    }


    public List<InterestRegion> getInterestRegions() {
        return regionManager.getInterestRegions();
    }


    public boolean removeInterestRegion(InterestRegion region) {
        boolean result = regionManager.removeInterestRegion(region);
        if (result) {
            HazardDetector hd = findHazardDetector(region);
            if (hd != null) {
                hazardDetectors.remove(hd);
            }
        }
        return result;
    }


    private void startHazardDetectorsTimer() {
        if (hazardDetectorsTimer != null) {
            stopHazardDetectorsTimer();
        }

        if (D) {
            Log.d(TAG, "Starting hazard detectors", null);
        }
        hazardDetectorsTimer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                runHazardDetectors();
            }
        };
        hazardDetectorsTimer.scheduleAtFixedRate(task, 0, HAZARD_DETECTORS_RUN_INTERVAL_SEC * 1000);
    }


    private void stopHazardDetectorsTimer() {
        if (hazardDetectorsTimer != null) {
            if (D) {
                Log.d(TAG, "Stopping hazard detectors", null);
            }
            hazardDetectorsTimer.cancel();
            hazardDetectorsTimer.purge();
            hazardDetectorsTimer = null;
        }
    }


    private void broadcast(String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        if (D) {
            Log.d(TAG, "Going to send broadcast event, action=" + action, null);
        }
        sendBroadcast(intent);
    }


    private void runHazardDetectors() {
        if (D) {
            if (hazardDetectors.size() > 0) {
                Log.d(TAG, "Starting runs of hazard detectors");
            } else {
                Log.d(TAG, "Starting runs of hazard detectors - getting bored, please add interest regions");
            }
        }
        for (HazardDetector hd : hazardDetectors) {
            new RunHazardDetectorTask(hd).execute();
        }
    }


    private class RunHazardDetectorTask extends AsyncTask<Void, Void, String> {

        private HazardDetector hazardDetector;


        public RunHazardDetectorTask(HazardDetector hazardDetector) {
            this.hazardDetector = hazardDetector;
        }


        @Override
        protected String doInBackground(Void... params) {
            if (D) {
                Log.d(TAG, "Running hazard detector for interest region " + hazardDetector.getInterestRegion().getLabel());
            }
            hazardDetector.run();
            return "done";
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (D) {
                Log.d(TAG, hazardDetector.report());
            }

            for (HazardEvent event : hazardDetector.getHazardEvents()) {
                if (event.isCritical() && !event.isReported()) {
                    event.setReported(true);

                    InterestRegion ir = hazardDetector.getInterestRegion();

                    if (D) {
                        Log.d(TAG, "Hazard event reached critical status, creating LookoutRegion for " + ir.getLabel());
                    }
                    final LookoutRegion lr = addLookoutRegion(ir.getLabel(), ir.getLatitude(), ir.getLongitude(), ir.getRadius());
                    lr.setDescription(ir.getDescription());
                    lr.setAlertStatus(LookoutRegion.ALERT_STATUS_YELLOW);

                    // save LookoutRegion to be validated
                    LookoutRegionManagerSingleton.saveLookoutRegion(lr, new OnSuccessCallback() {
                        @Override
                        public void onSuccess(Object result) {
                            // push notification to fire workers within region
                            InstallationManagerSingleton.INSTANCE.sendPushNotificationToProfessionalAt(lr);
                        }
                    }, null);

                    broadcast(NEW_LOOKOUT_REGION);
                }
            }
        }
    }

}
