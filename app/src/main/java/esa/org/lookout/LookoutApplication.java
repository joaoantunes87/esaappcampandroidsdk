package esa.org.lookout;

import android.app.Application;

import com.parse.Parse;
import com.parse.PushService;

import esa.org.lookout.backend.InstallationManagerSingleton;
import esa.org.lookout.ui.UiMain;

/**
 * Created by joaoantunes on 09/09/2014.
 */
public class LookoutApplication extends Application {

    @Override
    public void onCreate() {
        Parse.initialize(this, "JFRlzdfFlRn3xZc5TuT6byUni3Mf5bTXrDZlaBBt", "8EuX3EaQZ3eEAgWE5oJmQNDbvJAuFCbxPxPoq6KR");
        // refresh installation with parse
        InstallationManagerSingleton.INSTANCE.refreshInstallation();
        PushService.setDefaultPushCallback(this, UiMain.class);
    }

}
