package esa.org.lookout.ui;

import android.content.Context;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import esa.org.lookout.geo.InterestRegion;
import esa.org.lookout.geo.LookoutRegion;

public class LookoutRegionDetails extends FragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    private final String TAG = LookoutRegionDetails.class.getSimpleName();

    private final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private InterestRegion ir;
    private LookoutRegion lr;
    private GoogleMap mMap;


    private void setUpStuffIfNeeded(LatLng ll) {
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFromRegionDetails)).getMap();
            mMap.getUiSettings().setAllGesturesEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 13));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookout_region_details);

        Bundle b = getIntent().getExtras();
        ir = b.getParcelable("interest_region");
        lr = b.getParcelable("lookout_region");

        // TODO Fix this. Inheritance?
        LatLng ll;
        if (ir != null) {
            ((TextView) findViewById(R.id.newRegLabel)).setText(ir.getLabel());
            ((TextView) findViewById(R.id.textView6)).setText("Resolving name...");
            ((TextView) findViewById(R.id.newRegDesc)).setText(ir.getDescription());
            ll = new LatLng(ir.getLatitude(), ir.getLongitude());
        } else {
            ((TextView) findViewById(R.id.newRegLabel)).setText(lr.getLabel());
            ((TextView) findViewById(R.id.textView6)).setText("Resolving name...");
            ((TextView) findViewById(R.id.newRegDesc)).setText(lr.getDescription());
            switch (lr.getAlertStatus()) {
                case LookoutRegion.ALERT_STATUS_GREEN:
                    ((ImageView) findViewById(R.id.detail_icon)).setImageResource(R.drawable.fire_green_icon);
                    break;
                case LookoutRegion.ALERT_STATUS_YELLOW:
                    ((ImageView) findViewById(R.id.detail_icon)).setImageResource(R.drawable.fire_yellow_icon);
                    break;
                case LookoutRegion.ALERT_STATUS_RED:
                    ((ImageView) findViewById(R.id.detail_icon)).setImageResource(R.drawable.fire_red_icon);
                    break;
            }
            ll = new LatLng(lr.getLatitude(), lr.getLongitude());
        }

        if (Geocoder.isPresent()) {
            (new GetAddressTask(this)).execute(ll);
        }

        setUpStuffIfNeeded(ll);
    }


    private class GetAddressTask extends AsyncTask<LatLng, Void, String> {
        private Context c;


        public GetAddressTask(Context c) {
            super();
            this.c = c;
        }


        @Override
        protected String doInBackground(LatLng... locations) {
            Geocoder geo = new Geocoder(c, Locale.getDefault());
            LatLng loc = locations[0];
            List<Address> addresses = null;

            try {
                addresses = geo.getFromLocation(loc.latitude, loc.longitude, 1);
            } catch (IOException ex) {
                return "Failed to retrieve address";
            } catch (IllegalArgumentException ex) {
                return "Failed to retrieve address";
            }

            if (addresses != null && addresses.size() > 0) {
                Address addr = addresses.get(0);
                String addressText = String.format(
                        "%s, %s, %s",
                        // If there's a street address, add it
                        addr.getMaxAddressLineIndex() > 0 ?
                                addr.getAddressLine(0) : "",
                        // Locality is usually a city
                        addr.getLocality(),
                        // The country of the address
                        addr.getCountryName());
                // Return the text
                return addressText;
            } else {
                return "Location not found";
            }
        }


        @Override
        protected void onPostExecute(String s) {
            ((TextView) findViewById(R.id.textView6)).setText(s);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (ir != null) {
            getMenuInflater().inflate(R.menu.lookout_region_details, menu);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            // TODO: use onActivityResult to indicate interest region should be removed
            // RegionManager.getInstance().removeInterestRegion(ir);
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Location services connected");
    }


    @Override
    public void onDisconnected() {
        Log.d(TAG, "Location services disconnected");
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Location services connection failed");

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */

        if (connectionResult.hasResolution()) {
            // Start an Activity that tries to resolve the error
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
        }
    }
}
