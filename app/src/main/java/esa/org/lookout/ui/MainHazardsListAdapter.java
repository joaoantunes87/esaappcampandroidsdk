package esa.org.lookout.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import esa.org.lookout.geo.InterestRegion;

public class MainHazardsListAdapter extends BaseAdapter {

    private Context c;
    private LayoutInflater mInflater;
    private List<InterestRegion> items;


    public MainHazardsListAdapter(Context c, List<InterestRegion> items) {
        this.c = c;
        this.mInflater = LayoutInflater.from(c);
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.size();
    }


    @Override
    public Object getItem(int i) {
        return items.get(i);
    }


    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.list_lookout_region, null);
            holder = new ViewHolder();
            holder.ll = (LinearLayout) view.findViewById(R.id.lookoutListClickableView);
            holder.img = (ImageView) view.findViewById(R.id.lookoutImage);
            holder.label = (TextView) view.findViewById(R.id.lookoutLabel);
            holder.description = (TextView) view.findViewById(R.id.lookoutDesc);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final InterestRegion lr = items.get(i);

        // TODO Use home/office icons
        /*switch (lr.getAlertStatus()) {
            case LookoutRegion.ALERT_STATUS_GREEN:
                holder.img.setImageResource(R.drawable.fire_green);
                break;
            case LookoutRegion.ALERT_STATUS_YELLOW:
                holder.img.setImageResource(R.drawable.fire_yellow);
                break;
            case LookoutRegion.ALERT_STATUS_RED:
                holder.img.setImageResource(R.drawable.fire_red);
                break;
        }*/

        holder.label.setText(lr.getLabel());
        holder.description.setText(lr.getDescription().length() > 35 ? lr.getDescription().substring(0, 35) + "..." : lr.getDescription());
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putParcelable("interest_region", lr);

                Intent i = new Intent(c, LookoutRegionDetails.class);
                i.putExtras(b);

                c.startActivity(i);
            }
        });

        return view;
    }


    static class ViewHolder {
        LinearLayout ll;
        ImageView img;
        TextView label, description;
    }
}
