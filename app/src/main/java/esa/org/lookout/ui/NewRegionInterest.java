package esa.org.lookout.ui;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import esa.org.lookout.geo.InterestRegion;

public class NewRegionInterest extends FragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    private final String TAG = NewRegionInterest.class.getSimpleName();

    private final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private EditText label, description;

    private InterestRegion ir;
    private LatLng ll;
    private GoogleMap mMap;


    private void setUpStuffIfNeeded() {
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFromRegionDetails)).getMap();
            mMap.getUiSettings().setAllGesturesEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 15));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_region_interest);

        Bundle b = getIntent().getExtras();
        ir = b.getParcelable("interest_region");

        // TODO: ir should not be null
        // other key could be lookout_region

        ll = new LatLng(ir.getLatitude(), ir.getLongitude());

        label = (EditText) findViewById(R.id.newRegLabel);
        description = (EditText) findViewById(R.id.newRegDesc);

        final Context c = this;
        findViewById(R.id.saveNewReg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String l = label.getText().toString();
                String d = description.getText().toString();
                if (l.trim().length() == 0) {
                    Toast.makeText(c, "Please fill in at least the label", Toast.LENGTH_LONG).show();
                } else {
                    if (ir != null) {
                        ir.setLabel(l);
                        ir.setDescription(d);
                        // TODO: update other ir fields?
                    }
                    // TODO: add OnActivityResult handler in UiMain to either add or ignore the new region
                    Bundle b = new Bundle();
                    b.putParcelable("interest_region", ir);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtras(b);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                    // RegionManager.getInstance().addInterestRegion(l, d, ll.latitude, ll.longitude, RegionManager.DEFAULT_RADIUS);
                }
            }
        });

        setUpStuffIfNeeded();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Location services connected");
    }


    @Override
    public void onDisconnected() {
        Log.d(TAG, "Location services disconnected");
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Location services connection failed");

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */

        if (connectionResult.hasResolution()) {
            // Start an Activity that tries to resolve the error
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
        }
    }
}
