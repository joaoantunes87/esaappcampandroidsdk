package esa.org.lookout.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;
import com.parse.ParseAnalytics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import esa.org.lookout.backend.InstallationManagerSingleton;
import esa.org.lookout.geo.InterestRegion;
import esa.org.lookout.geo.LookoutRegion;
import esa.org.lookout.geo.RegionManager;
import esa.org.lookout.geo.georss.GeoRSSpiceRequest;
import esa.org.lookout.geo.georss.GeoRSSpiceService;
import esa.org.lookout.geo.georss.rush.Feeds;
import esa.org.lookout.service.LookoutService;
import esa.org.lookout.utils.UpDownAnim;

public class UiMain extends FragmentActivity implements LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, RequestListener<Feeds> {
    public static final String HTTP_GEORSS_FEEDS_URL = "http://feeds.feedburner.com/CopernicusEMSMappingRushModeActivations";

    // TODO Terminate location services when no longer in use to save battery

    private final String TAG = UiMain.class.getSimpleName();

    private final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private final int MILLISECONDS_PER_SECOND = 1000;
    private final int UPDATE_INTERVAL_IN_SECONDS = 60;
    private final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    private final int FASTEST_INTERVAL_IN_SECONDS = 10;
    private final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    private boolean listVisible = false;
    private boolean clicked = false;
    private boolean zoomed = false;

    private List<Marker> lookoutMarkers;
    private List<Circle> lookoutCircles;
    private Map<Marker, LookoutRegion> markerToLookout;

    private InterestRegion yourCurrentInterestRegion;

    private Button addNewRegionButton;
    private Marker mMarker;
    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;

    private MainHazardsListAdapter mAdapter;

    private final SpiceManager geoRSSpiceManager = new SpiceManager(GeoRSSpiceService.class);
    private GeoRSSpiceRequest geoRSSpiceRequest;


    private Intent lookoutServiceIntent;
    private LookoutService lookoutServiceBinder;


    /**
     * Connect to the background LookoutService.
     */
    private ServiceConnection lookoutConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            lookoutServiceBinder = ((LookoutService.LookoutServiceBinder) service).getService();
            setUpStuffIfNeeded();
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            lookoutServiceBinder = null;
        }
    };


    private BroadcastReceiver lookoutBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LookoutService.NEW_LOOKOUT_REGION.equals(intent.getAction())) {
                updateMapMarkers();
            }
        }
    };


    private void startLookoutService(Intent intent) {
        // Bundle extras = intent.getExtras();

        // TODO: show progress window?
        try {
            Intent bindIntent = new Intent(UiMain.this, LookoutService.class);
            bindService(bindIntent, lookoutConnection, Context.BIND_AUTO_CREATE);

            lookoutServiceIntent = new Intent(this, LookoutService.class);
            startService(lookoutServiceIntent);
        } catch (Exception ex) {
            Log.e(TAG, "Could not start LookoutService");
            // TODO: show nice message window
        }
    }


    private boolean stopLookoutService() {
        if (lookoutServiceBinder != null) {
            lookoutServiceBinder.stopService();
            try {
                unbindService(lookoutConnection);
            } catch (IllegalArgumentException ex) {
            }
        } else {
            if (lookoutServiceIntent != null) {
                return stopService(lookoutServiceIntent);
            }
        }
        return true;
    }


    private void setUpStuffIfNeeded() {
        final Context c = this;
        ParseAnalytics.trackAppOpened(this.getIntent());

        if (mMap == null) {
            addNewRegionButton = (Button) findViewById(R.id.acceptButton);

            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            mMap.setMyLocationEnabled(true);

            addNewRegionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle b = new Bundle();
                    InterestRegion ir = InterestRegion.createRegion(
                            "Add a title",
                            "Add a description",
                            mMarker.getPosition().latitude,
                            mMarker.getPosition().longitude,
                            InterestRegion.DEFAULT_RADIUS);

                    b.putParcelable("interest_region", ir);

                    Intent i = new Intent(c, NewRegionInterest.class);
                    i.putExtras(b);
                    startActivityForResult(i, 1);
                }
            });

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    clearMarker();
                }
            });

            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    clicked = true;
                    if (mMarker != null) {
                        mMarker.setPosition(latLng);
                    } else {
                        mMarker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        addNewRegionButton.setVisibility(Button.VISIBLE);
                    }
                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    LookoutRegion lr = markerToLookout.get(marker);
                    Bundle b = new Bundle();
                    b.putParcelable("lookout_region", lr);

                    Intent i = new Intent(c, LookoutRegionDetails.class);
                    i.putExtras(b);

                    c.startActivity(i);
                    return true;
                }
            });

            mLocationRequest = LocationRequest.create();
            mLocationClient = new LocationClient(this, this, this);
        }

        if (mLocationRequest != null) {
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        }

        if ((mAdapter == null) && (lookoutServiceBinder != null)) {
            mAdapter = new MainHazardsListAdapter(this, lookoutServiceBinder.getInterestRegions());
            ListView l = (ListView) findViewById(R.id.lookoutsListView);
            l.setAdapter(mAdapter);
        }

        LinearLayout slider = (LinearLayout) findViewById(R.id.mainSliderList);
        slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpDownAnim anim = new UpDownAnim(c, view, 220, !listVisible);
                anim.setDuration(500);
                view.startAnimation(anim);
                ImageView arrow = (ImageView) findViewById(R.id.mainSliderArrow);
                arrow.setImageResource(listVisible ? R.drawable.arrow_up : R.drawable.arrow_down);
                listVisible = !listVisible;
            }
        });

        EditText text = (EditText) findViewById(R.id.searchMap);
        text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    clearMarker();
                    clicked = true;
                    Log.d(TAG, "Searching for: " + textView.getText());
                    if (Geocoder.isPresent()) {
                        try {
                            Geocoder geo = new Geocoder(c);
                            List<Address> addresses = geo.getFromLocationName(textView.getText().toString(), 1);
                            if (addresses.size() > 0) {
                                Address addr = addresses.get(0);
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(addr.getLatitude(), addr.getLongitude()), 14));
                            } else {
                                Toast.makeText(c, "Location not found, sorry.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException ex) {
                            Toast.makeText(c, "Location not found, sorry.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }


    private void clearMarker() {
        if (mMarker != null) {
            clicked = false;
            mMarker.remove();
            mMarker = null;
            addNewRegionButton.setVisibility(Button.INVISIBLE);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui_main);

        startLookoutService(getIntent());

        // TODO: stuff that needs the lookout service might not be available yet
        setUpStuffIfNeeded();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        geoRSSpiceRequest = new GeoRSSpiceRequest(HTTP_GEORSS_FEEDS_URL);
        geoRSSpiceRequest = new GeoRSSpiceRequest("http://feeds.feedburner.com/CopernicusEMSMappingRushModeActivations");

        /* HACK FOR GENYMOTION */
        ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpStuffIfNeeded();
        clearMarker();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(LookoutService.NEW_LOOKOUT_REGION);

        registerReceiver(lookoutBroadcastReceiver, filter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                InterestRegion ir = b.getParcelable("interest_region");
                lookoutServiceBinder.addInterestRegion(ir.getLabel(), ir.getLatitude(), ir.getLongitude(), ir.getRadius()).setDescription(ir.getDescription());
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            } else if(resultCode == RESULT_CANCELED) {

            }
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(lookoutBroadcastReceiver);
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLookoutService();
    }


    @Override
    protected void onStart() {
        geoRSSpiceManager.start(this);
        super.onStart();

        mLocationClient.connect();
        Log.d(TAG, "Location connection requested");

        //async geoRSS data download start here
        getGeoRSSpiceManager().execute(geoRSSpiceRequest, this);

        // start the service that runs the hazard detector
        Intent intent = new Intent(this, LookoutService.class);
        startService(intent);
    }


    @Override
    protected void onStop() {
        if (mLocationClient.isConnected()) {
            mLocationClient.removeLocationUpdates(this);
            mLocationClient.disconnect();
            Log.d(TAG, "Stopping location services");
        }

        if (geoRSSpiceManager.isStarted()) {
            geoRSSpiceManager.shouldStop();
        }

        stopLookoutService();

        super.onStop();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ui_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_exit) {
            stopLookoutService();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Location services connected");
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }


    @Override
    public void onDisconnected() {
        Log.d(TAG, "Location services disconnected");
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Updated Location: " + Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude()));
        Log.d(TAG, "Accuracy: " + location.getAccuracy());
        Log.d(TAG, "Provider: " + location.getProvider());

        if (yourCurrentInterestRegion == null) {
            yourCurrentInterestRegion = lookoutServiceBinder.addSpecialInterestRegion("You", location.getLatitude(), location.getLongitude(), RegionManager.DEFAULT_RADIUS);
            yourCurrentInterestRegion.setDescription("Your current location as last retrieved by the device");
            mAdapter.notifyDataSetChanged();
        }

        yourCurrentInterestRegion.setLatitude(location.getLatitude());
        yourCurrentInterestRegion.setLongitude(location.getLongitude());

        if (location.getAccuracy() < 20) {
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL);
        }

        LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
        if (clicked || zoomed) {
            return;
        }
        if (!zoomed) {
            zoomed = true;
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 12));
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLng(ll));
        }

        // updating current location to the backend
        InstallationManagerSingleton.INSTANCE.saveCurrentInstallationLocation(ll.latitude, ll.longitude);

        // check the current temperature status on the area
        /*
        try {
            LandSurfaceTemperatureManagerSingleton.INSTANCE.saveTemperatureAt(ll.latitude, ll.longitude);
        } catch (NoResultsException noResultException) {
            // TODO
        }
        */

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Location services connection failed");

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */

        if (connectionResult.hasResolution()) {
            // Start an Activity that tries to resolve the error
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
        }
    }


    @Override
    public void onRequestFailure(com.octo.android.robospice.persistence.exception.SpiceException spiceException) {

    }


    //Method where parsed GeoRSS data are provided
    @Override
    public void onRequestSuccess(Feeds feeds) {
        //Toast.makeText(this, "GeoRSS downloaded successfully", Toast.LENGTH_LONG).show();
        if (lookoutServiceBinder != null) {
            lookoutServiceBinder.addLookoutRegions(feeds);
        }

        updateMapMarkers();
    }


    private void updateMapMarkers() {
        if (lookoutMarkers != null) {
            for (int i = 0; i < lookoutMarkers.size(); i++) {
                lookoutMarkers.get(i).remove();
                lookoutCircles.get(i).remove();
            }
        }

        List<LookoutRegion> lookoutRegions = lookoutServiceBinder.getLookoutRegions();
        lookoutMarkers = new ArrayList<Marker>(lookoutRegions.size());
        lookoutCircles = new ArrayList<Circle>(lookoutRegions.size());

        markerToLookout = new HashMap<Marker, LookoutRegion>();

        for (LookoutRegion region : lookoutRegions) {
            MarkerOptions mo = new MarkerOptions().position(new LatLng(region.getLatitude(), region.getLongitude()));
            CircleOptions co = new CircleOptions().center(new LatLng(region.getLatitude(), region.getLongitude())).radius(region.getRadius()).strokeWidth(2);

            switch (region.getAlertStatus()) {
                case LookoutRegion.ALERT_STATUS_GREEN:
                    mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.fire_green));
                    co.fillColor(Color.argb(120, 0, 255, 0)).strokeColor(Color.GREEN);
                    break;
                case LookoutRegion.ALERT_STATUS_YELLOW:
                    mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.fire_yellow));
                    co.fillColor(Color.argb(120, 255, 255, 0)).strokeColor(Color.YELLOW);
                    break;
                case LookoutRegion.ALERT_STATUS_RED:
                    mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.fire_red));
                    co.fillColor(Color.argb(120, 255, 0, 0)).strokeColor(Color.RED);
                    break;
            }

            Marker m = mMap.addMarker(mo);

            lookoutMarkers.add(m);
            lookoutCircles.add(mMap.addCircle(co));
            markerToLookout.put(m, region);
        }
    }


    protected SpiceManager getGeoRSSpiceManager() {
        return geoRSSpiceManager;
    }

}
